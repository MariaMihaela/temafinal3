#ifndef __AVL__H__
#define __AVL__H__
#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include<vector>
#include<string>
using namespace std;

struct Node {
    	string movie_id;
    	//string movie_id;
	int timestamp;
	std::vector<std::string> categories;
	//string director_name;
    	//std::vector<std::string> actor_ids;
	//std::vector<std::pair<std::string, std::string> > ratings;
    	struct Node *left;
    	struct Node *right;
    	int height;
};
 
int height(struct Node *N){
    if (N == NULL)
        return 0;
    return N->height;
}

int max(int a, int b) {
    return (a > b)? a : b;
}
 

struct Node* newNode(std::string movie_id,
                     int timestamp,
                     std::vector<std::string> categories
                     //std::string director_name,
                     /*std::vector<std::string> actor_ids*/) {
    struct Node* node = new struct Node;
    node->movie_id = movie_id;
    //node->movie_name = movie_name; 
    node->timestamp = timestamp;
    node->categories.swap(categories);
    //node->director_name = director_name;
    //node->actor_ids.swap(actor_ids);	
    node->left   = NULL;
    node->right  = NULL;
    node->height = 1;  
    return(node);
}
  

struct Node *rightRotate(struct Node *y) {
    struct Node *x = y->left;
    struct Node *T2 = x->right;
 
  
    x->right = y;
    y->left = T2;
 
    y->height = max(height(y->left), height(y->right))+1;
    x->height = max(height(x->left), height(x->right))+1;
 
    return x;
}
 

struct Node *leftRotate(struct Node *x)
{
    struct Node *y = x->right;
    struct Node *T2 = y->left;

    y->left = x;
    x->right = T2;
 
  
    x->height = max(height(x->left), height(x->right))+1;
    y->height = max(height(y->left), height(y->right))+1;
 
    return y;
}
 
int getBalance(struct Node *N)
{
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}
 

struct Node* insert(struct Node* node, string movie_id,
			 int timestamp,
			std::vector<std::string> categories
                     /*	std::string director_name,/*
                    /*	std::vector<std::string> actor_ids*/)
{
    if (node == NULL)
        return(newNode(movie_id, /*movie_name,*/ timestamp, categories/*, director_name, actor_ids*/));
 
    if (timestamp < node->timestamp)
        node->left  = insert(node->left, movie_id,/* movie_name,*/ timestamp, categories/*, director_name, actor_ids*/);
    else if (timestamp > node->timestamp)
        node->right = insert(node->right, movie_id,/* movie_name,*/ timestamp, categories/*, director_name, actor_ids*/);
    else
        return node;
 
    node->height = 1 + max(height(node->left),
                           height(node->right));
 
    int balance = getBalance(node);

    if (balance > 1 && timestamp < node->left->timestamp)
        return rightRotate(node);
 
    if (balance < -1 && timestamp > node->right->timestamp)
        return leftRotate(node);
 
    if (balance > 1 && timestamp > node->left->timestamp)
    {
        node->left =  leftRotate(node->left);
        return rightRotate(node);
    }
 
    if (balance < -1 && timestamp < node->right->timestamp)
    {
        node->right = rightRotate(node->right);
        return leftRotate(node);
    }
 
    return node;
}
 

void preOrder(struct Node *root)
{
    if(root != NULL)
    {
        printf("%d ", root->timestamp);
        preOrder(root->left);
        preOrder(root->right);
    }
}
 

int findPositionPreOrder(
    struct Node *root,
    int targetPos,
    int curPos)
{
    if(root != NULL)
    {
        int newPos = findPositionPreOrder(root->left, targetPos, curPos);
        newPos++;
        if (newPos == targetPos)
        {
            printf("%d\n", root->timestamp);
        }
        return findPositionPreOrder(root->right, targetPos, newPos);
    }

    else
    {
        return curPos;
    }
}



/*int main()
{
  struct Node *root = NULL;
 long int x;
 ifstream f("numbers.txt");
  for (int i = 1;i <= 4000000; i++){
	f >> x;
  root = insert(root, x);
  }

 // for (int i=1;i<=500;i++)
//	findPositionPreOrder(root,i,1);
 
  preOrder(root);
 
  return 0;
}*/
#endif
