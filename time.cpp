
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "./include/time.h"


int get_year(int timestamp) {

	time_t t= timestamp;
	struct tm *lt = gmtime( &t);
	return (lt->tm_year + 1900);
	
}


int get_mon(int timestamp) {

	time_t t = timestamp;
	struct tm *lt = gmtime( &t);
	return (lt->tm_mon + 1);
	
}

int get_day(int timestamp) {

	time_t t= timestamp;
	struct tm *lt = gmtime( &t);
	return (lt->tm_mday);
	
}

