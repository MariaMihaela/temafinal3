#ifndef __CATEGORII__H__
#define __CATEGORII__H__
#include <map>
#include <string>
#include <vector>
using namespace std;

struct film {
	string movie_id;
	int sum;
	int nr_ratings;
};

typedef std::vector<film> filme_per_ani;
//x filme_per_ani;//vector cu toate filmele dintr-un an
typedef std::vector<filme_per_ani> ani;
//y ani; //vectori de ani care contine toate filmele dintr-un an;

std::map <std::string, ani > Categorii;
//hashtable cu cheia = categoria si valoarea un vector de ani care contine filmele din acea categorie ordonate dupa ani;





#endif
