
//#include"structuri.h"
#include<iostream>
 using namespace std;

class Heap
{ 
private:
    int* values;
    std::string *movie_name; // toti vectorii vor avea dimensiunea dimVect
    std::string *movie_id; 
    int dimVect;
    int capVect;
public:
    Heap(int capVect);
    Heap();
    ~Heap();
 
    int parent(int poz);
 
    int leftSubtree(int poz);
 
    int rightSubtree(int poz);

    void delete_element(int poz);
 
    void pushUp(int poz);
 
    void pushDown(int poz);
    
    void print();
 
    void insert(int x,string y,string z);
   
    int Search(int nr,string nume,int poz,int rating_nou,int actiune);

   // void update(string movie_name, string movie_id, int rating_vechi,int rating_nou);
	
   int* returnare_vector();  //cred ca e int, inainte era T
    
    void HeapSort();

    int peek(); //cred ca e int, inainte era T
 
    //int extractMin();  //cred ca e int, inainte era T
};


Heap::Heap(){}

Heap::Heap(int capVect)
{
   values=new int[capVect+1];
   movie_id=new string[capVect+1]; //s.ar putea sa fie capVect+1 pt ca stii ca pozitia 0 tb sa ramana libera
   movie_name=new string[capVect+1];
  this->capVect=capVect+1;
   dimVect=1;
}

Heap::~Heap()
{
    delete[] values;
    delete[] movie_name;
    delete[] movie_id;
}

int Heap::parent(int poz)
{
    return (poz)/2;
}


int Heap::leftSubtree(int poz)
{
    return  2 * poz ;
}


int Heap::rightSubtree(int poz)
{
    return 2 * poz +1 ;
}

void Heap::pushUp(int l) {
            int parent;
            int vaux;
	    string vauxx;

            parent = (l)/2;
            while (values[parent] > values[l]) {
					// if not top of heap and if H[parent] and
					// H[l] do not respect the heap’s order
                vaux = values[parent];
                values[parent] = values[l];	// interchange H[parent] and H[l]
                values[l] = vaux;
		
		vauxx=movie_name[parent];
		movie_name[parent] = movie_name[l];	// interchange H[parent] and H[l]
                movie_name[l] = vauxx;
		vauxx=movie_id[parent];
		movie_id[parent] = movie_id[l];	// interchange H[parent] and H[l]
                movie_id[l] = vauxx;
		


                l = parent;	// move l to point to the parent
                parent = (l)/2;
            }
}

void Heap::pushDown(int poz) {
            int l =poz;
            int vaux;
	    string vauxx;
            while (1) {
                if (2 * l + 1 > dimVect) {   // if there isn’t a right son
                    if (2 * l > dimVect)     // if there isn’t a left son
                        break;
                    else if (values[2 * l] < values[l]) { // if there is a left son and
						     // it’s not heap proper order
                        vaux = values[2 * l];        // interchange the node and
                        values[2 * l] = values[l];        // it’s left son
                        values[l] = vaux;
			vauxx = movie_name[2 * l];        // interchange the node and
                        movie_name[2 * l] = movie_name[l];        // it’s left son
                        movie_name[l] = vauxx;
			vauxx = movie_id[2 * l];        // interchange the node and
                        movie_id[2 * l] = movie_id[l];        // it’s left son
                       movie_id[l] = vauxx;
                        l = 2 * l;		     // and move to the left son

                    }
                    else                        // there is a left son but 
                        break;                  // it’s heap proper order
                }
	else { // there are both a left son and a right son
           if (values[2 * l] <= values[2 * l + 1] && values[2 * l]< values[l]) {
		  // if left son is smaller and it’s not in heap proper order
               vaux = values[2 * l];
               values[2 * l] = values[l];     // interchange the node and it’s left son
               values[l] = vaux;
		 vauxx = movie_name[2 * l];
               movie_name[2 * l] = movie_name[l];     // interchange the node and it’s left son
               movie_name[l] = vauxx;
		 vauxx = movie_id[2 * l];
               movie_id[2 * l] = movie_id[l];     // interchange the node and it’s left son
               movie_id[l] = vauxx;
               l = 2 * l;           // move on the left son
		
           }
           else if (values[2 * l + 1]<= values[2 * l] && values[2 * l + 1] < values[l]) {
           // if right son is smaller than right and it’s not in heap order
               vaux = values[2 * l + 1];
               values[2 * l + 1] = values[l]; // interchange the node and it’s right son
               values[l] = vaux;
               l = 2 * l + 1;       // move to the right son
		vauxx = movie_name[2 * l + 1];
               movie_name[2 * l + 1] = movie_name[l]; // interchange the node and it’s right son
               movie_name[l] = vauxx;
               l = 2 * l + 1;   
		vauxx = movie_id[2 * l + 1];
               movie_id[2 * l + 1] = movie_id[l]; // interchange the node and it’s right son
               movie_id[l] = vauxx;
               l = 2 * l + 1;   
		
           }
           else			  // all nodes in proper order
               break;         
           }
       }
    }
void Heap::insert(int x,string y,string z)
{
  if(dimVect<capVect) {values[dimVect] = x;
			movie_name[dimVect]=y;
			movie_id[dimVect]=z;	
  			dimVect++;
  pushUp(dimVect-1);
  print();
  cout<<endl;
 }
}


void Heap::print()
{
for(int i=0;i<capVect;i++)
	std::cout<<values[i]<<' ';
}
int Heap::peek()
{            /////////////////???????????????????????am nevoie de toate campurile sau doar de rating?
    return values[0];
}
void Heap::HeapSort(){
	
int x=dimVect;
    for (int i = x; i >= 1; i--) 
    {
        // Punem maximul la sfarsitul vectorului
        //interschimba(heap[0], heap[i]); // interschimba primul element cu ultimul si apoi il aranjeaza in heap dupa maxim. daca e maxim va fi primul daca nu va cobori
	int temp=values[0];
	string tempp=movie_name[0];
	string temppp=movie_id[0];
	values[0]=values[i];
	movie_name[0]=movie_name[i];
	movie_id[0]=movie_id[i];
	movie_name[i]=tempp;
	movie_id[i]=temppp;
	values[i]=temp;
        // 'Desprindem' maximul de heap (valoarea ramanand astfel in pozitia finala)
        x--;
        // Reconstituim heap-ul ramas
	//cout<<"inceput"<<endl;
        pushDown(0);
	//cout<<"sfarsit"<<endl;
			cout<<endl;


    }
}
//tb sa ma gandesc si la cazul cand nu o sa mai am deloc elementul acolo.
//la delete verific ratingul daca e 0. daca e 0 atunci dau delete.
int Heap::Search(int nr,string nume,int poz,int rating_nou,int actiune){
if(nr==values[poz]&& nume==movie_name[poz]) {
	if(actiune==1) { //update
			//facem update
			values[poz]=rating_nou;
			}
	if(actiune==2){ delete_element(poz);} //aici am actiunea de stergere
return poz;} //am gasit
if(nr<=values[poz]) Search(nr,nume,2*poz,rating_nou,actiune);
else if(nr>values[poz]) Search(nr,nume,2*poz+1,rating_nou,actiune);
return 0; //nu am gasit
}

int* Heap::returnare_vector(){
	return values;
}
void Heap:: delete_element(int poz){

    while(true)
    {
        int left_child = leftSubtree(poz);
	int right_child =rightSubtree(poz);
        if(left_child <= dimVect)
        {
            if(right_child <= dimVect)
            {
		if(values[poz] > values[left_child] || values[poz] > values[right_child])
			{
			//int index_nou = (values[left_child] < values[right_child])? left_child:right_child;
               		pushUp(poz);
			pushDown(poz);
			//poz = index_nou;
			}
		else break;
		}
            else
            {
                if(values[poz] > values[left_child])
                { //aici fac swap doar cu pointerul stang
		  // poz = left_child;
                    pushUp(poz);
		    pushDown(poz);

                }
                else
                    break;
            }
        }
        else
            break;
    }

}
/*void insertElement(T x) {
            if (currentDim == maxDim) {	  // no more room
                fprintf(stderr, "Error!\n");
                return;
            }
            currentDim++;
            H[currentDim] = x;
            pushUp(currentDim);		  // this does most of the work
		}*/

/*T Heap<T>::extractMin()
{
  int temp=values[0];
  values[0]=values[dimVect - 1];
  values[dimVect-1]=temp;
  dimVect--;
  pushDown(0,dimVect);
}*/
int main(){
Heap H(9);
H.insert(40,"Gheorghe","a");
H.insert(60,"Darius","b");
H.insert(10,"Calin","c");
H.insert(20,"Cara","d");
H.insert(50,"Cristofor","e");
H.insert(30,"Laurentiu","f");
H.HeapSort();
cout<<"HOPAAA"<<endl;
H.print();
int *v=H.returnare_vector();
}
//Heap<Actor_heap> Career_heap; //aici retin actorii dupa cea mai lunga cariera;
//Heap Top_rating;

