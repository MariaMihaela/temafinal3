
#include<iostream>
#include<vector>
#include<tuple>
 using namespace std;

class Heap_pairs
{ 
private:
    std::vector<int> values;
    std::vector<std::string> actor_id1; // toti vectorii vor avea dimensiunea dimVect
    std::vector<std::string> actor_id2; 
    int dimVect;
public:
    Heap();
    Heap(int);

    ~Heap();
    void regenerare();
	
    int parent(int poz);
 
    int leftSubtree(int poz);
 
    int rightSubtree(int poz);
 
    void pushUp(int poz);
 
    void pushDown(int poz,int dimVect);
    
    void print();
 
    void insert(int x,string y,string z);

    void delete_value(int val);

    void HeapSort();

    int peek();
 
    int extractMin();
	void extract(std::vector<std::tuple<int,string,string> > &v ,int & );
};


Heap::Heap(){}
Heap::Heap(int x)
{
   dimVect=x;
 //am vectori din stl
  values.push_back(0);
 actor_id1.push_back("");
  actor_id2.push_back("");
  
}

Heap::~Heap()
{	
}


int Heap::parent(int poz)
{
    return (poz)/2;
}


int Heap::leftSubtree(int poz)
{
    return  2 * poz ;
}


int Heap::rightSubtree(int poz)
{
    return 2 * poz +1 ;
}
void Heap::pushUp(int l) {
            int parent;
            int vaux;
	    string vauxx;
		std::cout<<l<<"Pozitie pushDOwn"<<endl;
            parent = (l)/2;
            while (values[parent] <values[l] && parent>0) {
					// if not top of heap and if H[parent] and
					// H[l] do not respect the heap’s order
                vaux = values[parent];
                values[parent] = values[l];	// interchange H[parent] and H[l]
                values[l] = vaux;
		vauxx=actor_id1[parent];
		actor_id1[parent]=actor_id1[l];
		actor_id1[l]=vauxx;
		vauxx=actor_id2[parent];
		actor_id2[parent]=actor_id2[l];
		actor_id2[l]=vauxx;
		

                l = parent;	// move l to point to the parent
                parent = (l)/2;
            }
        }
 

void Heap::pushDown(int poz,int dimVect) {
            int l =poz;
            int vaux;
	   string vauxx;

            while (1) {
                if (2 * l + 1 > dimVect) {   // if there isn’t a right son
                    if (2 * l > dimVect)     // if there isn’t a left son
                        break;

                    else if (2*l<=dimVect && values[2 * l] > values[l]) {  // if there is a left son and
						     // it’s not heap proper order
		cout<<"VIIIIIIIIIIIIIIIIIIIN ACUM"<<endl;
 
			vaux = values[2 * l];
               		 values[2 * l] = values[l];	// interchange H[parent] and H[l]
               		 values[l] = vaux;
			vauxx=actor_id1[2 * l];
			actor_id1[2 * l]=actor_id1[l];
			actor_id1[l]=vauxx;
			vauxx=actor_id2[2 * l];
			actor_id2[2 * l]=actor_id2[l];
			actor_id2[l]=vauxx;
			l=2*l;
				     // and move to the left son
		print();
		cout<<endl;

                    }
                    else   {    		cout<<" naspaaaaaaaaaaaaaaaVIIIIIIIIIIIIIIIIIIIN"<<endl;                // there is a left son but 
                        break;    }              // it’s heap proper order
                }
	else {if (values[2 * l + 1]> values[2 * l] && values[2 * l + 1] >values[l]) {		cout<<values[2*l+1]<<" l="<<" "<<l<<" "<<values[2*l]<<endl;
           // if right son is smaller than right and it’s not in heap order
               vaux = values[2 * l + 1];
               values[2 * l + 1] = values[l]; // interchange the node and it’s right son
               values[l] = vaux;
		vauxx=actor_id1[2 * l+1];
			actor_id1[2 * l+1]=actor_id1[l];
			actor_id1[l]=vauxx;
			vauxx=actor_id2[2 * l+1];
			actor_id2[2 * l+1]=actor_id2[l];
			actor_id2[l]=vauxx;
               l = 2 * l + 1;       // move to the right son
	print();
	cout<<endl;
		
           } // there are both a left son and a right son
          
           else if (values[2 * l] >= values[2 * l + 1] && values[2 * l]> values[l]) {
			cout<<"VIIIIIIIIIIIIIIIIIIINe primavaraaaaaaaaa"<<endl;		
		  // if left son is smaller and it’s not in heap proper order
               vaux = values[2 * l];
               values[2 * l] = values[l];     // interchange the node and it’s left son
               values[l] = vaux;
		vauxx=actor_id1[2 * l];
			actor_id1[2 * l]=actor_id1[l];
			actor_id1[l]=vauxx;
			vauxx=actor_id2[2 * l];
			actor_id2[2 * l]=actor_id2[l];
			actor_id2[l]=vauxx;
	    // if(2*l>dimVect && values[l]==-1) values[l]=0;	
               l = 2 * l;           // move on the left son
	print();
		cout<<endl;
	
		
           }
           else	
			  // all nodes in proper order
               break;       
           }

       }
    }

void Heap::insert(int x,string y,string z)
{	

			values.push_back(x);
			actor_id1.push_back(y);
			actor_id2.push_back(z);	
  			dimVect++;
		cout<<dimVect<<"DIMVECT"<<endl;

  pushUp(dimVect-1);
  print();
  cout<<endl;

 
}

void Heap::print()
{
for(int i=1;i<dimVect;i++)
	cout<<values[i]<<" "<<actor_id1[i]<<" "<<actor_id2[i]<<endl;
cout<<endl;
}

int Heap::peek()
{
    return values[0];
}

void Heap::HeapSort(){	
    for (int i = dimVect; i >= 1; i--) 
    {
        // Punem maximul la sfarsitul vectorului
        //interschimba(heap[0], heap[i]); // interschimba primul element cu ultimul si apoi il aranjeaza in heap dupa maxim. daca e maxim va fi primul daca nu va cobori
	int temp=values[0];
	values[0]=values[i];
	values[i]=temp;
        // 'Desprindem' maximul de heap (valoarea ramanand astfel in pozitia finala)
        dimVect--;
        // Reconstituim heap-ul ramas
	print();
	cout<<"inceput"<<endl;
        pushDown(0,dimVect);
	print();
	cout<<"sfarsit"<<endl;
			cout<<endl;

    }



}
void Heap::extract(std::vector<std::tuple<int,string,string> > &v,int& dim){
	/*M[dim].values=this->values[0];
	M[dim].movie_name=this->movie_name[0]; //////////////////////si aici tb sa modific cu ambele nume
	M[dim].movie_id=this-> movie_id[0];*/
 	std::tuple <int,string,string> foo;
	string x;
	string y;
	if(actor_id1[1]<actor_id2[1]) {x=actor_id1[1];y=actor_id2[1];}
		else {x=actor_id2[1];y=actor_id1[1];}
	foo=std::make_tuple(values[1],x,y);
	v.push_back(foo);
		for(int i=dim;i>=1;i--){
			if( std::get<0>(v[i])== std::get<0>(v[i-1])){ 
				if( std::get<1>(v[i])< std::get<1>(v[i-1])){
					if( std::get<2>(v[i])<std::get<2>(v[i-1]))
							v[i].swap(v[i-1]);
										}
						
						
						}
					}
		dim++; //cred ca se incrementeaza la sfarsit ca mearga
	/*for(int i=1;i<=dim;i++)
		cout<<v[i].first<<"Aici"<<v[i].second<<endl;*/
cout<<"Diimvectorrrrrrrrrrrrrrrrrrrr"<<dimVect<<endl;
	int temp=values[1];
	string tempp=actor_id1[1];
	values[1]=values[dimVect-1];
	values[dimVect-1]=temp;
	actor_id1[1]=actor_id1[dimVect-1];
	actor_id1[dimVect-1]=tempp;
	tempp=actor_id2[1];
	actor_id2[1]=actor_id2[dimVect-1];
	actor_id2[dimVect-1]=tempp;
	cout<<endl;
	print();
	dimVect--;	
	cout<<"push down:"<<endl;		
	pushDown(1,dimVect-1);
	print();

	
}



/*
int main(){
Heap H(1);
H.insert(40,"Gheorghe","Maria");
H.insert(60,"Darius","Codrut");
H.insert(40,"Dana","Crina");
H.insert(40,"Gheorghe","Cristian");
H.insert(10,"Calin","Sia");
H.insert(20,"Cara","Ortansa");
std::vector<std::tuple<int,string,string> > v;
std::tuple<int,string,string> naspa;
    v.emplace_back(0,"","");
int dim=0;
int k=5;
while(k>=1){
H.extract(v,dim); k--;}

cout<<"ACIIIIIIIIIIIIIIIIIIIIIIIIII"<<endl;
for(int i=1;i<=dim;i++)
	cout<<std::get<0>(v[i])<<" "<<std::get<1>(v[i])<<" "<<std::get<2>(v[i])<<endl;
cout<<endl;
}*/
Heap_pairs Actor_pairs(1);
