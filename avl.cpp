#include<stdio.h>
#include<stdlib.h> 
#include<fstream>
#include<vector>
#include<string>
#include"./include/avll.h"
#include"./include/time.h"
using namespace std;

int height(struct Node *N){
    if (N == NULL)
        return 0;
    return N->height;
}

int max(int a, int b) {
    return (a > b)? a : b;
}
 
struct Node* newNode(std::string movie_id,
                     int timestamp
                     //std::string director_name,
                     /*std::vector<std::string> actor_ids*/) {
    struct Node* node = new struct Node;
    node->movie_id = movie_id;
    //node->movie_name = movie_name; 
    node->timestamp = timestamp;
    //node->director_name = director_name;
    //node->actor_ids.swap(actor_ids);	
    node->left   = NULL;
    node->right  = NULL;
    node->height = 1;  
	//cout<<"AM ADAUGAT IN AVL PE"<<node->movie_id<<" "<<node->timestamp<<endl;
    return(node);
}
  

struct Node *rightRotate(struct Node *y) {
    struct Node *x = y->left;
    struct Node *T2 = x->right;
 
  
    x->right = y;
    y->left = T2;
 
    y->height = max(height(y->left), height(y->right))+1;
    x->height = max(height(x->left), height(x->right))+1;
 
    return x;
}
 

struct Node *leftRotate(struct Node *x)
{
    struct Node *y = x->right;
    struct Node *T2 = y->left;

    y->left = x;
    x->right = T2;
 
  
    x->height = max(height(x->left), height(x->right))+1;
    y->height = max(height(y->left), height(y->right))+1;
 
    return y;
}
 
int getBalance(struct Node *N)
{
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}
 

struct Node* insert(struct Node* node, string movie_id,
			 int timestamp)
{
    if (node == NULL)
        return(newNode(movie_id, timestamp));
 
    if (timestamp <= node->timestamp)
        node->right  = insert(node->right, movie_id,/* movie_name,*/ timestamp);
    else if (timestamp >= node->timestamp)
        node->left = insert(node->left, movie_id,/* movie_name,*/ timestamp);
    else
        return node;
 
    node->height = 1 + max(height(node->left),
                           height(node->right));
 
    int balance = getBalance(node);

    
    if (balance > 1 && timestamp >= node->left->timestamp)
        return rightRotate(node);
 
    if (balance < -1 && timestamp <= node->right->timestamp)
        return leftRotate(node);
 
    if (balance > 1 && timestamp <= node->left->timestamp)
    {
        node->left =  leftRotate(node->left);
        return rightRotate(node);
    }
 
     if (balance < -1 && timestamp >= node->right->timestamp)
    {
        node->right = rightRotate(node->right);
        return leftRotate(node);
    }
 
    return node;
}
 /*
void/*nuj de ce tip e*/  /*Search_in_range(struct Node *root,int start,int stop,std::vector<std::string> &V,int &contor){
//1.transforma timestampul in an;
//2.cauta daca e in range
//3.returneaza id.ul filmului in range
int x=root->timestamp;
int y= get_year(x); //MODIFICA CU FUNCTIA ADAUGATA DE RUXANDRA
if(y>=start && y<= stop) {
	V[contor]=root->movie_id;contor++;
	Search_in_range(root->left,start,stop,V,contor);
	Search_in_range(root->right,start,stop,V,contor);
}
else if(y<=start) Search_in_range(root->right,start,stop,V,contor);
else if(y>=stop) Search_in_range(root->left,start,stop,V,contor); 


 





}*/
void Search_in_range(struct Node *root,int start,int stop,std::vector<std::string> &V,int &contor)
{
if(root!=nullptr){
if(root->timestamp!=NULL) {int x=root->timestamp;
if(x>=start && x <=stop )
	 {std::string hopa=root->movie_id;
	 V.push_back(hopa);
	 cout<<root->movie_id<<" ";contor++;}}
if(root->right!=nullptr) Search_in_range(root->right,start,stop,V,contor);
if(root->left!=nullptr)  Search_in_range(root->left,start,stop,V,contor);

}	
}


void  InOrder(struct Node *root,int contor,int k,std::vector<std::string> &x)
{
   
      if(root->left!=NULL) InOrder(root->left,contor,k,x);
       if(contor<k) {x[contor]=root->movie_id;contor++;//cout<<contor;
	}
       if(root->right!=NULL) InOrder(root->right,contor,k,x);
   
}
void printInorder(struct Node* node,std::vector<std::pair<std::string,int> > &x,int contor,int k)
{
     if (node == NULL)
          return;
 
     /* first recur on left child */
    if(node->left!=NULL && contor<=k) printInorder(node->left,x,contor,k);
 
     /* then print the data of node */
	if(contor<k) {
	std::pair<std::string,int> k;
	k.first=node->movie_id;
	k.second=node->timestamp;
	x.push_back(k);
	int pozitie=x.size();
	//************************push_up();**************************
	if(pozitie>=1){
		for(int i=x.size()-1;i>=0;i--)
		if(x[pozitie].second==x[i].second && x[pozitie].first>x[i].first) { x[i].swap(x[pozitie]); pozitie=i;}
		}
	
	contor++;
	}

 

     /* now recur on right child */
    if(node->right!=NULL && contor<=k) printInorder(node->right,x,contor,k);
}
 
/*
int findPositionPreOrder(
    struct Node *root,
    int targetPos,
    int curPos)
{
    if(root != NULL)
    {
        int newPos = findPositionPreOrder(root->left, targetPos, curPos);
        newPos++;
        if (newPos == targetPos)
        {
            printf("%d\n", root->timestamp);
        }
        if(root->right)  return findPositionPreOrder(root->right, targetPos, newPos);
    }

    else
    {
        return curPos;
    }
}*/
