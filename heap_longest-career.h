#ifndef HEAP_H
#define HEAP_H

#include<iostream>
 
template <typename T>
class Heap
{ 
private:
    T* values;
    int dimVect;
    int capVect;
public:
    Heap(int capVect);
    ~Heap();
 
    int parent(int poz);
 
    int leftSubtree(int poz);
 
    int rightSubtree(int poz);
 
    void pushUp(int poz);
 
    void pushDown(int poz);
    
    void print();
 
    void insert(T x);
 
    T peek();
 
    T extractMin();
};



template <typename T>
Heap<T>::Heap(int capVect)
{
   values=new T[capVect];
  this->capVect=capVect;
   dimVect=0;
}

template <typename T>
Heap<T>::~Heap()
{
    delete[] values;
}

/* TODO Exercitiul 2 */

template <typename T>
int Heap<T>::parent(int poz)
{
    return (poz-1)/2;
}

template <typename T>
int Heap<T>::leftSubtree(int poz)
{
    return  2 * poz + 1 ;
}

template <typename T>
int Heap<T>::rightSubtree(int poz)
{
    return 2 * poz +2 ;
}

template <typename T>
void Heap<T>::pushUp(int poz)
{// verifica ca valoarea adaugata e mai mica decat parintele
	T x=parent(poz);
	if(values[poz]>values[x]) {T temp=values[poz];
			values[poz]=values[x];
			values[x]=temp;			
			pushUp(x);}
	
 
}

template <typename T>
void Heap<T>::pushDown(int poz)
{//valoarea este mai mare cel putin decat unul dintre descendenti deci trebuie sa fie mai mica decat ambii descendenti
	T x=parent(poz);
	T stg=leftSubtree(x);
	T dr=rightSubtree(x);
	if(values[x]<values[stg] || values[x]<values[dr]) {int temp=values[poz];
			values[poz]=values[x];
			values[x]=temp;			
			pushDown(x);}

 
}

template <typename T>
void Heap<T>::insert(T x)
{
  if(dimVect<capVect) {values[dimVect] = x;
  			dimVect++;
  pushUp(dimVect - 1);
 }
}
template <typename T>
void Heap<T>::print()
{
for(int i=0;i<capVect;i++)
	std::cout<<values[i]<<' ';
}
template <typename T>
T Heap<T>::peek()
{
    return values[0];
}

template <typename T>
T Heap<T>::extractMin()
{
  T temp=values[0];
  values[0]=values[dimVect - 1];
  values[dimVect-1]=temp;
  dimVect--;
  pushDown(0);
}

#endif // HEAP_H
