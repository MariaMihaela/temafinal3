
//#include"structuri.h"
#include<iostream>
#include<vector>
 using namespace std;

class Heap
{ 
private:
    std::vector<int> values;
    std::vector<std::string> movie_name; // toti vectorii vor avea dimensiunea dimVect
    std::vector<std::string> movie_id; 
    int dimVect;
    int capacitate;
public:
    Heap();
    Heap(int);

    ~Heap();
    void regenerare();
	
    int parent(int poz);

    int leftSubtree(int poz);
 
    int rightSubtree(int poz);
 
    void pushUp(int poz);
 
    void pushDown(int poz,int dimVect);
    
    void print();
 
    void insert(int x,string y,string z);

    void delete_value(int val);

    void HeapSort();

    int peek();
 
    int extractMin();
    int extract(std::vector<std::pair<int,string> > &v ,int & );
};


Heap::Heap(){ }
Heap::Heap(int k)
{
   capacitate = k;
   dimVect = 1;
 //am vectori din stl
  values.push_back(0);
  movie_name.push_back("");
  movie_id.push_back("");
  
}

Heap::~Heap()
{	
}


int Heap::parent(int poz)
{
    return (poz)/2;
}


int Heap::leftSubtree(int poz)
{
    return  2 * poz ;
}


int Heap::rightSubtree(int poz)
{
    return 2 * poz +1 ;
}
void Heap::regenerare(){
values.clear();
movie_name.clear();
movie_id.clear();
dimVect=1;
values.push_back(0);
  movie_name.push_back("");
  movie_id.push_back("");
cout<<"REGENERZ"<<endl;
for (auto it = values.begin() ; it !=values.end(); ++it)
	cout<<(*it)<<" ";
for (auto it = movie_name.begin() ; it !=movie_name.end(); ++it)
	cout<<(*it)<<" ";
for (auto it = movie_id.begin() ; it !=movie_id.end(); ++it)
	cout<<(*it)<<" ";

cout<<"AM REGENERAT"<<endl;
}

void Heap::pushUp(int l) {
            int parent;
            int vaux;
	    string vauxx;
		//std::cout<<l<<"Pozitie pushDOwn"<<endl;
            parent = (l)/2;
            while (values[parent] > values[l] && parent>0) {
					// if not top of heap and if H[parent] and
					// H[l] do not respect the heap’s order
                vaux = values[parent];
                values[parent] = values[l];	// interchange H[parent] and H[l]
                values[l] = vaux;
		vauxx=movie_name[parent];
		movie_name[parent]=movie_name[l];
		movie_name[l]=vauxx;
		vauxx=movie_id[parent];
		movie_id[parent]=movie_id[l];
		movie_id[l]=vauxx;
		

                l = parent;	// move l to point to the parent
                parent = (l)/2;
            }
        }
 

void Heap::pushDown(int poz,int dimVect) {
            int l =poz;
            int vaux;
	   string vauxx;
	   cout<<"valoarea"<<values[poz]<<endl;
            while (1) {
                if (2 * l + 1 > dimVect) {   // if there isn’t a right son
                    if (2 * l > dimVect)     // if there isn’t a left son
                        break;

                    else if (2*l<=dimVect && values[2 * l] < values[l]) {  // if there is a left son and
						     // it’s not heap proper order
		

			vaux = values[2 * l];
               		 values[2 * l] = values[l];	// interchange H[parent] and H[l]
               		 values[l] = vaux;
			vauxx=movie_name[2 * l];
			movie_name[2 * l]=movie_name[l];
			movie_name[l]=vauxx;
			vauxx=movie_id[2 * l];
			movie_id[2 * l]=movie_id[l];
			movie_id[l]=vauxx;
			l=2*l;
				     // and move to the left son
		print();
		//cout<<endl;

                    }
                    else   {    		//cout<<" naspaaaaaaaaaaaaaaaVIIIIIIIIIIIIIIIIIIIN"<<endl;                // there is a left son but 
                        break;    }              // it’s heap proper order
                }
	else {
			if (values[2 * l + 1] <= values[2 * l] && values[2 * l + 1] < values[l]) {		//cout<<values[2*l+1]<<" l="<<" "<<l<<" "<<values[2*l]<<endl;            //************ trebuie sa urce cel mai mic element din cei doi vecini 
			//*********** deci va fi 2l+1< 2l si values 2 l+ 1 < values l
				// *********** copilul tb sa fie mereu mai mic
			cout<<"aloo";
             vaux = values[2 * l + 1];
             values[2 * l + 1] = values[l]; // interchange the node and it’s right son
             values[l] = vaux;

			vauxx=movie_name[2 * l+1];
			movie_name[2 * l+1]=movie_name[l];
			movie_name[l]=vauxx;

			vauxx=movie_id[2 * l+1];
			movie_id[2 * l+1]=movie_id[l];
			movie_id[l]=vauxx;
            l = 2 * l + 1;       // move to the right son
	
	//cout<<endl;
		
           } // there are both a left son and a right son
          
           else if (values[2 * l] <= values[2 * l + 1] && values[2 * l]< values[l]) {
		//	cout<<"VIIIIIIIIIIIIIIIIIIINe primavaraaaaaaaaa"<<endl;		
		  // if left son is smaller and it’s not in heap proper order
               vaux = values[2 * l];
               values[2 * l] = values[l];     // interchange the node and it’s left son
               values[l] = vaux;
			vauxx=movie_name[2 * l];
			movie_name[2 * l]=movie_name[l];
			movie_name[l]=vauxx;
			vauxx=movie_id[2 * l];
			movie_id[2 * l]=movie_id[l];
			movie_id[l]=vauxx;
	    // if(2*l>dimVect && values[l]==-1) values[l]=0;	
               l = 2 * l;           // move on the left son
	print();
		//cout<<endl;
	
		
           }
           else	
			  // all nodes in proper order
               break;       
           }

       }
    }

void Heap::insert(int x, string y, string z)
{			cout<<"inceput"<<x<<" ";
			if (dimVect > capacitate) {
			
				cout<<x<<" "<<values[1]<<endl;
				if (x > values[1]){
		
				values[1] = x;
				movie_name[1] = y;
				movie_id[1] = z;
					
				pushDown(1,capacitate);

 			//	print();
 			}
 			}

 				else {
					values.push_back(x);
					movie_name.push_back(y);
					movie_id.push_back(z);	
  					dimVect ++;
  					pushUp(dimVect - 1);
 				}

 				cout<<"sfarsit"<<" ";
 				print();
}

void Heap::print()
{
cout<<"printez ce am in heap"<<endl;
for(int i=0;i<dimVect;i++)
	std::cout<<values[i]<<' ';
}

int Heap::peek()
{
    return values[0];
}

void Heap::HeapSort(){	
    for (int i = dimVect; i >= 1; i--) 
    {
        // Punem maximul la sfarsitul vectorului
        //interschimba(heap[0], heap[i]); // interschimba primul element cu ultimul si apoi il aranjeaza in heap dupa maxim. daca e maxim va fi primul daca nu va cobori
	int temp=values[0];
	values[0]=values[i];
	values[i]=temp;
        // 'Desprindem' maximul de heap (valoarea ramanand astfel in pozitia finala)
        dimVect--;
        // Reconstituim heap-ul ramas
	print();
		cout<<endl;
	//cout<<"inceput"<<endl;
        pushDown(0,dimVect);
	//print();
	//cout<<"sfarsit"<<endl;
			//cout<<endl;

    }



}
struct camp_rating{
 int values;
    std::string movie_name; // toti vectorii vor avea dimensiunea dimVect
    std::string movie_id; 
    int dimVect;
	/*camp_rating(){
	values=new int;
	movie_name=new string;
	movie_id=new string;}*/
	camp_rating(int x,string y,string z){
	//values=new int;
	values=x;
	//movie_name=new string;
	movie_name=y;
	//movie_id=new string;
	movie_id=z;
}
	/*~camp_rating(){
	delete values;
	delete movie_name;
	delete movie_id;}*/
	
};
int Heap::extract(/*struct camp_rating *M*/std::vector<std::pair<int,string> > &v,int& dim){
	/*M[dim].values=this->values[0];
	M[dim].movie_name=this->movie_name[0];
	M[dim].movie_id=this-> movie_id[0];*/
 	if(dimVect > 	1) { 
	std::pair <int,string> foo;
	foo=std::make_pair(values[1],movie_id[1]);
cout<<"UITE-MA"<<endl;
cout<<values[1]<<" "<<movie_id[1]<<endl;
	v.push_back(foo);
		for(int i=dim;i>=0;i--){
			if(v[i].first==v[i-1].first){ 
				if(v[i].second<v[i-1].second){
					v[i].swap(v[i-1]);}
						
						
						}
					}
		dim++; //cred ca se incrementeaza la sfarsit ca mearga
	/*for(int i=1;i<=dim;i++)
		cout<<v[i].first<<"Aici"<<v[i].second<<endl;*/
//cout<<"Diimvectorrrrrrrrrrrrrrrrrrrr"<<dimVect<<endl;
	int temp=values[1];
	string tempp=movie_name[1];
	values[1]=values[dimVect-1];
	values[dimVect-1]=temp;
	movie_name[1]=movie_name[dimVect-1];
	movie_name[dimVect-1]=tempp;
	tempp=movie_id[1];
	movie_id[1]=movie_id[dimVect-1];
	movie_id[dimVect-1]=tempp;
	//cout<<endl;

	cout<<endl;
	dimVect--;
	/*cout<<"printez de la extract"<<endl;
	print();*/	
	//cout<<"push down:"<<endl;		
	pushDown(1,dimVect-1);
	return 1;}
return 0;
	//print();

	
}

int Heap::extractMin()
{
  int temp=values[0];
  values[0]=values[dimVect - 1];
  values[dimVect-1]=temp;
  dimVect--;
  pushDown(0,dimVect);
}

void Heap:: delete_value(int poz){

    while(true)
    {
        int left_child = leftSubtree(poz);
	int right_child =rightSubtree(poz);
        if(left_child <= dimVect)
        {
            if(right_child <= dimVect)
            {
		if(values[poz] > values[left_child] || values[poz] > values[right_child])
			{
			//int index_nou = (values[left_child] < values[right_child])? left_child:right_child;
               		pushUp(poz);
			pushDown(poz,dimVect);
			//poz = index_nou;
			}
		else break;
		}
            else
            {
                if(values[poz] > values[left_child])
                { //aici fac swap doar cu pointerul stang
		  // poz = left_child;
                    pushUp(poz);
		    pushDown(poz,dimVect);

                }
                else
                    break;
            }
        }
        else
            break;
    }

}


/*
int main(){
Heap H(5);
H.insert(40,"Gheorghe","a");
H.insert(60,"Darius","b");
H.insert(40,"Dana","a");
H.insert(40,"Andra","b");
H.insert(10,"Calin","c");
H.insert(20,"Cara","d");
H.insert(10,"Alin","e");
H.insert(30,"Irina","f");
H.print();
/*std::vector<std::pair<int,string> > v;
std::pair<int,string> naspa;
naspa.first=0;
naspa.second="";
v.push_back(naspa);
int dim=0;

cout<<"ACIIIIIIIIIIIIIIIIIIIIIIIIII"<<endl;
for(int i=1;i<=dim;i++)
	cout<<v[i].first<<" "<<v[i].second<<endl;
cout<<endl;
//H.insert(50,"Cristofor","e");
//H.insert(30,"Laurentiu","f");
//H.insert(80);
//H.insert(84);
H.print();
///H.delete_value();
std::vector<std::pair<int,string> > v;
std::pair<int,string> naspa;
naspa.first=0;
naspa.second="";
v.push_back(naspa);
//am introdus un element pe prima pozitie ca sa poata merge totul incepand cu pozitia 1
int k=5;
int dim=0;
while(k>=1){
H.extract(v,dim); k--;}
cout<<"ESTE NASPAaaaaaaaaAAAAAAAAAAAAAAAAAAAAa"<<endl;
for(int i=1;i<=4;i++)
	cout<<v[i].first<<" "<<v[i].second<<endl;
cout<<"Aa"<<endl;
//H.HeapSort();
H.print();}
*/
//Heap Top_Rating(1);
//}
