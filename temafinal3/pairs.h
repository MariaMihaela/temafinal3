
#include<iostream>
#include<vector>
#include<tuple>
using namespace std;

class Heap_pairs
{
private:
    std::vector<int> values;
    std::vector<std::string> actor_id1; // toti vectorii vor avea dimensiunea dimVect
    std::vector<std::string> actor_id2;
    int dimVect;
    int capacitate;
public:
    Heap_pairs();
    Heap_pairs(int);

    ~Heap_pairs();

    void regenerare();

    int parent(int poz);

    int leftSubtree(int poz);

    int rightSubtree(int poz);

    void pushUp(int poz);

    void pushDown(int poz,int dimVect);

    void print();

    void insert(int x,string y,string z);

    void delete_value(int val);

    void HeapSort();

    int peek();

    int extractMin();
    int extract(std::vector<std::tuple<int,string,string> > &v,int & );
};


Heap_pairs::Heap_pairs() {}
Heap_pairs::Heap_pairs(int k)
{
    dimVect=1;
//am vectori din stl
    values.push_back(0);
    actor_id1.push_back("");
    actor_id2.push_back("");
    capacitate = k;

}

Heap_pairs::~Heap_pairs()
{
}


int Heap_pairs::parent(int poz)
{
    return (poz)/2;
}


int Heap_pairs::leftSubtree(int poz)
{
    return  2 * poz ;
}


int Heap_pairs::rightSubtree(int poz)
{
    return 2 * poz +1 ;
}
void Heap_pairs::pushUp(int l) {
    int parent;
    int vaux;
    string vauxx;
    std::cout<<l<<"Pozitie pushDOwn"<<endl;
    parent = (l)/2;
while(1){
    if(values[parent] >values[l] && parent>0) {
        // if not top of heap and if H[parent] and
        // H[l] do not respect the heap’s orde
        vaux = values[parent];
        values[parent] = values[l];	// interchange H[parent] and H[l]
        values[l] = vaux;
        vauxx=actor_id1[parent];
        actor_id1[parent]=actor_id1[l];
        actor_id1[l]=vauxx;
        vauxx=actor_id2[parent];
        actor_id2[parent]=actor_id2[l];
        actor_id2[l]=vauxx;


        l = parent;	// move l to point to the parent
        parent = (l)/2;
    }
    else if(values[parent]==values[l]){
	if(actor_id1[parent]>actor_id1[l]){
	vaux = values[parent];
        values[parent] = values[l];	// interchange H[parent] and H[l]
        values[l] = vaux;
        vauxx=actor_id1[parent];
        actor_id1[parent]=actor_id1[l];
        actor_id1[l]=vauxx;
        vauxx=actor_id2[parent];
        actor_id2[parent]=actor_id2[l];
        actor_id2[l]=vauxx;


        l = parent;	// move l to point to the parent
        parent = (l)/2;	}
	else if(actor_id1[parent]== actor_id1[l])
		if(actor_id2[parent]>actor_id2[l]){
			vaux = values[parent];
     		       values[parent] = values[l];	// interchange H[parent] and H[l]
       			 values[l] = vaux;
      			  vauxx=actor_id1[parent];
       			 actor_id1[parent]=actor_id1[l];
       			 actor_id1[l]=vauxx;
      		         vauxx=actor_id2[parent];
                         actor_id2[parent]=actor_id2[l];
                         actor_id2[l]=vauxx;
		         l = parent;	// move l to point to the parent
       			 parent = (l)/2;	}

					}
	else break;
		}
}


void Heap_pairs::pushDown(int poz,int dimVect) {
    int l =poz;
    int vaux;
    string vauxx;

    while (1) {
        if (2 * l + 1 > dimVect) {   // if there isn’t a right son
            if (2 * l > dimVect)     // if there isn’t a left son
                break;

            else if (2*l<=dimVect && values[2 * l] < values[l]) {  // if there is a left son and
                //aici arre un singur copil -copil stangaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa
                cout<<"VIIIIIIIIIIIIIIIIIIIN ACUM"<<endl;
                vaux = values[2 * l];
                values[2 * l] = values[l];
                values[l] = vaux;
                vauxx=actor_id1[2 * l];
                actor_id1[2 * l]=actor_id1[l];
                actor_id1[l]=vauxx;
                vauxx=actor_id2[2 * l];
                actor_id2[2 * l]=actor_id2[l];
                actor_id2[l]=vauxx;
                l=2*l;
                //print();
                //cout<<endl;
            }
            else if (2*l<=dimVect && values[2 * l] == values[l]) {	 //are doar copil stanga
                if(actor_id1[2*l] > actor_id1[l]) {
                    vaux = values[2 * l];
                    values[2 * l] = values[l];	// interchange H[parent] and H[l]
                    values[l] = vaux;
                    vauxx=actor_id1[2 * l];
                    actor_id1[2 * l]=actor_id1[l];
                    actor_id1[l]=vauxx;
                    vauxx=actor_id2[2 * l];
                    actor_id2[2 * l]=actor_id2[l];
                    actor_id2[l]=vauxx;
                    l=2*l;
                }
                else if (actor_id1[2*l] ==actor_id1[l]) {
                    if(actor_id2[2*l] >actor_id2[l]) {
                        vaux = values[2 * l];
                        values[2 * l] = values[l];	// interchange H[parent] and H[l]
                        values[l] = vaux;
                        vauxx=actor_id1[2 * l];
                        actor_id1[2 * l]=actor_id1[l];
                        actor_id1[l]=vauxx;
                        vauxx=actor_id2[2 * l];
                        actor_id2[2 * l]=actor_id2[l];
                        actor_id2[l]=vauxx;
                        l=2*l;
                        //break;
                    }
                }
                print();
                cout<<endl;
            }

        }
        //else if( 2*l +1  > dimVect && 2*l > dimVect) break;

        else {// sunt in interior cu ambii copii

            if (values[2 * l + 1]<= values[2 * l] && values[2 * l + 1] <values[l]) {

                vaux = values[2 * l + 1];
                values[2 * l + 1] = values[l]; // interchange the node and it’s right son
                values[l] = vaux;
                vauxx=actor_id1[2 * l+1];
                actor_id1[2 * l+1]=actor_id1[l];
                actor_id1[l]=vauxx;
                vauxx=actor_id2[2 * l+1];
                actor_id2[2 * l+1]=actor_id2[l];
                actor_id2[l]=vauxx;
                l = 2 * l + 1;       // move to the right son
                cout<<"AIC SUNT .SAx`LVEAZA-MAAAAAAAAAAAAAAAAAAAAA"<<endl;
                print();
                cout<<endl;
		cout<<"COPIL DREAPTA SE INTERSCHIMBA CU TATAL"<<endl;
                //MAI TREBUIE CONDITIILE DE IDENTIFICARE A NUMELUI MAI MARE
            } // there are both a left son and a right son


            else if (values[2 * l + 1]== values[2 * l] && values[2 * l + 1] == values[l]) {
	cout<<"CAZUL VALORILOR EGALE PE TOATE NODURILE"<<endl;
                if(actor_id1[2*l+1] > actor_id1[2*l] && actor_id1[2*l+1]>actor_id1[l]) { //
                    // if right son is smaller than right and it’s not in heap order
                    vaux = values[2 * l + 1];
                    values[2 * l + 1] = values[l]; // interchange the node and it’s right son
                    values[l] = vaux;
                    vauxx=actor_id1[2 * l+1];
                    actor_id1[2 * l+1]=actor_id1[l];
                    actor_id1[l]=vauxx;
                    vauxx=actor_id2[2 * l+1];
                    actor_id2[2 * l+1]=actor_id2[l];
                    actor_id2[l]=vauxx;
                    l = 2 * l + 1;       // move to the right son
                    print();
                    cout<<endl;
			cout<<"INTERSCHIMB DREAPTA CU TATAL -val egale"<<endl;
                }
                else if(actor_id1[2*l] >actor_id1[2*l+1] && actor_id1[2*l]> actor_id1[l]) {
                    // if right son is smaller than right and it’s not in heap order
                    vaux = values[2 * l ];
                    values[2 * l ] = values[l]; // interchange the node and it’s right son
                    values[l] = vaux;
                    vauxx=actor_id1[2 * l];
                    actor_id1[2 * l]=actor_id1[l];
                    actor_id1[l]=vauxx;
                    vauxx=actor_id2[2 * l];
                    actor_id2[2 * l]=actor_id2[l];
                    actor_id2[l]=vauxx;
                    l = 2 * l ;       // move to the right son
                    print();
                    cout<<endl;
				cout<<"INTERSCHIMB stanga CU TATAL -val egale"<<endl;
                }
                else if(actor_id1[2*l+1] == actor_id1[l])
                {   if (actor_id2[2*l+1] > actor_id2[2*l] && actor_id2[2*l+1]> actor_id2[l])
                    {
                        // if right son is smaller than right and it’s not in heap order
                        vaux = values[2 * l + 1];
                        values[2 * l + 1] = values[l]; // interchange the node and it’s right son
                        values[l] = vaux;
                        vauxx=actor_id1[2 * l+1];
                        actor_id1[2 * l+1]=actor_id1[l];
                        actor_id1[l]=vauxx;
                        vauxx=actor_id2[2 * l+1];
                        actor_id2[2 * l+1]=actor_id2[l];
                        actor_id2[l]=vauxx;
                        l = 2 * l + 1;       // move to the right son
                        print();
                        cout<<endl;
			cout<<"egale 1 schimb cu dreapta 2"<<endl;
                    }
                    if (actor_id2[2*l] > actor_id2[l] && actor_id2[2*l]> actor_id2[l])
                    {
                        // if right son is smaller than right and it’s not in heap order
                        vaux = values[2 * l ];
                        values[2 * l ] = values[l]; // interchange the node and it’s right son
                        values[l] = vaux;
                        vauxx=actor_id1[2 * l];
                        actor_id1[2 * l]=actor_id1[l];
                        actor_id1[l]=vauxx;
                        vauxx=actor_id2[2 * l];
                        actor_id2[2 * l]=actor_id2[l];
                        actor_id2[l]=vauxx;
                        l = 2 * l ;       // move to the right son
                        print();
                        cout<<endl;
						cout<<"egale 1 schimb cu stanga 2"<<endl;
                    }
                }

         cout<<"GATA VALORI EGALE"<<endl;
		   }
            else if (values[2 * l]< values[2 * l + 1] && values[2 * l]< values[l]) {
                cout<<"VIIIIIIIIIIIIIIIIIIINe primavaraaaaaaaaa"<<endl;
                // if left son is smaller and it’s not in heap proper order
                vaux = values[2 * l];
                values[2 * l] = values[l];     // interchange the node and it’s left son
                values[l] = vaux;
                vauxx=actor_id1[2 * l];
                actor_id1[2 * l]=actor_id1[l];
                actor_id1[l]=vauxx;
                vauxx=actor_id2[2 * l];
                actor_id2[2 * l]=actor_id2[l];
                actor_id2[l]=vauxx;
                // if(2*l>dimVect && values[l]==-1) values[l]=0;
                l = 2 * l;           // move on the left son
                print();
                cout<<"AICI IMI DA LOOP"<<endl;
                break;
            }
          /*  else if (values[2 * l ]<values[2 * l+1] && values[2 * l ] == values[l]) {
                if(actor_id1[2*l] > actor_id1[l]) {
                    // if right son is smaller than right and it’s not in heap order
                    vaux = values[2 * l ];
                    values[2 * l ] = values[l]; // interchange the node and it’s right son
                    values[l] = vaux;
                    vauxx=actor_id1[2 * l];
                    actor_id1[2 * l]=actor_id1[l];
                    actor_id1[l]=vauxx;
                    vauxx=actor_id2[2 * l];
                    actor_id2[2 * l]=actor_id2[l];
                    actor_id2[l]=vauxx;
                    l = 2 * l;       // move to the right son
                    print();
                    cout<<endl;
                    break;
                }
                else if(actor_id1[2*l] == actor_id1[l])
                    if (actor_id2[2*l] > actor_id2[l])
                    {
                        // if right son is smaller than right and it’s not in heap order
                        vaux = values[2 * l ];
                        values[2 * l ] = values[l]; // interchange the node and it’s right son
                        values[l] = vaux;
                        vauxx=actor_id1[2 * l];
                        actor_id1[2 * l]=actor_id1[l];
                        actor_id1[l]=vauxx;
                        vauxx=actor_id2[2 * l];
                        actor_id2[2 * l]=actor_id2[l];
                        actor_id2[l]=vauxx;
                        l = 2 * l;       // move to the right son
                        print();
                        cout<<endl;
                        break;
                    }

            }*/
        
                // all nodes in proper order
         break;
	}

    }
}

void Heap_pairs::insert(int x,string y,string z)
{  std::string temp;
 if (y>z) {
temp = y;
y= z;
z = temp;
}
    if (dimVect > capacitate) {

        //cout<<values[1]<<endl;
        if (x > values[1]) {

            values[1] = x;
            actor_id1[1] = y;
            actor_id2[1] = z;

            pushDown(1,capacitate-1);

        }
    }

    else {
        values.push_back(x);
        actor_id1.push_back(y);
        actor_id2.push_back(z);
        dimVect++;
        pushUp(dimVect - 1);
    }

}


void Heap_pairs::print()
{
    for(int i=1; i<dimVect; i++)
        cout<<values[i]<<" "<<actor_id1[i]<<" "<<actor_id2[i]<<endl;
    cout<<endl;
}

int Heap_pairs::peek()
{
    return values[1];
}

void Heap_pairs::HeapSort() {
    for (int i = dimVect; i >= 1; i--)
    {
        // Punem maximul la sfarsitul vectorului
        //interschimba(heap[0], heap[i]); // interschimba primul element cu ultimul si apoi il aranjeaza in heap dupa maxim. daca e maxim va fi primul daca nu va cobori
        int temp=values[0];
        values[0]=values[i];
        values[i]=temp;
        // 'Desprindem' maximul de heap (valoarea ramanand astfel in pozitia finala)
        dimVect--;
        // Reconstituim heap-ul ramas
        //print();
        cout<<"inceput"<<endl;
        // pushDown(0,dimVect);
        //print();
        //cout<<"sfarsit"<<endl;
        cout<<endl;

    }

}
void Heap_pairs::regenerare() {
    values.clear();
    actor_id1.clear();
    actor_id2.clear();
    dimVect=1;
    values.push_back(0);
    actor_id1.push_back("");
    actor_id2.push_back("");




}

int Heap_pairs::extract(std::vector<std::tuple<int,string,string> > &v,int& dim) {
    /*M[dim].values=this->values[0];
    M[dim].movie_name=this->movie_name[0]; //////////////////////si aici tb sa modific cu ambele nume
    M[dim].movie_id=this-> movie_id[0];*/
    if(dimVect > 	1) {
        std::tuple <int,string,string> foo;
        std::string x;
        std::string y;
        cout<<actor_id1[1]<<" "<<actor_id2[1]<<endl;
        cout<<"Mai sus "<<endl;
        if(actor_id1[1]<actor_id2[1]) {
            x=actor_id1[1];
            y=actor_id2[1];
        }
        else {
            x=actor_id2[1];
            y=actor_id1[1];
            cout<<"AICI ";
        }
        cout<<x<<" "<<y;
        foo=std::make_tuple(values[1],x,y);
        v.push_back(foo);
        for(int i=dim; i>=1; i--) {
            if( std::get<0>(v[i])== std::get<0>(v[i-1])) {
                if( std::get<1>(v[i])< std::get<1>(v[i-1])) {
                    //if( std::get<2>(v[i])<std::get<2>(v[i-1]))
                    v[i].swap(v[i-1]);
                }
                else if ( std::get<1>(v[i])== std::get<1>(v[i-1]))
                    if( std::get<2>(v[i])<std::get<2>(v[i-1]))
                        v[i].swap(v[i-1]);



            }
        }
        dim++; //cred ca se incrementeaza la sfarsit ca mearga
        /*for(int i=1;i<=dim;i++)
        	cout<<v[i].first<<"Aici"<<v[i].second<<endl;*/
        cout<<"Diimvectorrrrrrrrrrrrrrrrrrrr"<<dimVect<<endl;
        int temp=values[1];
        string tempp=actor_id1[1];
        values[1]=values[dimVect-1];
        values[dimVect-1]=temp;
        actor_id1[1]=actor_id1[dimVect-1];
        actor_id1[dimVect-1]=tempp;
        tempp=actor_id2[1];
        actor_id2[1]=actor_id2[dimVect-1];
        actor_id2[dimVect-1]=tempp;
        cout<<endl;
        //print();
        dimVect--;
        //cout<<"push down:"<<endl;
        pushDown(1,dimVect-1);
        print();
        cout<<endl;

        return 1;
    }
    return 0;
}




/*
int main(){
Heap H(1);
H.insert(40,"Gheorghe","Maria");
H.insert(60,"Darius","Codrut");
H.insert(40,"Dana","Crina");
H.insert(40,"Gheorghe","Cristian");
H.insert(10,"Calin","Sia");
H.insert(20,"Cara","Ortansa");
std::vector<std::tuple<int,string,string> > v;
std::tuple<int,string,string> naspa;
    v.emplace_back(0,"","");
int dim=0;
int k=5;
while(k>=1){
H.extract(v,dim); k--;}

cout<<"ACIIIIIIIIIIIIIIIIIIIIIIIIII"<<endl;
for(int i=1;i<=dim;i++)
	cout<<std::get<0>(v[i])<<" "<<std::get<1>(v[i])<<" "<<std::get<2>(v[i])<<endl;
cout<<endl;
}*/

