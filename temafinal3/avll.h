#ifndef __AVLL__H__
#define __AVLL__H__
#include<stdio.h>
#include<stdlib.h>
#include<fstream>
#include<vector>
#include<string>
using namespace std;
/*
struct Node {
    	string movie_id;
    	//string movie_id;
	int timestamp;
	std::vector<std::string> categories;
	//string director_name;
    	//std::vector<std::string> actor_ids;
	//std::vector<std::pair<std::string, std::string> > ratings;
    	struct Node *left;
    	struct Node *right;
    	int height;
};
 */
struct Node {
    	string movie_id;
    	//string movie_id; 
	int timestamp;
	//string director_name;
    	//std::vector<std::string> actor_ids;
	//std::vector<std::pair<std::string, std::string> > ratings;
    	struct Node *left;
    	struct Node *right;
    	int height;
	Node(){
	left=nullptr;
	right=nullptr;
	timestamp=0;
	height=0;}
	~Node(){
	if(left!=NULL) delete left;
	if(right!=NULL) delete right;
	left=nullptr;
	right=nullptr;
	}
};
 
int height(struct Node *N);

int max(int a, int b);
 
struct Node* newNode(std::string movie_id,
                     int timestamp
                     //std::string director_name,
                     /*std::vector<std::string> actor_ids*/);
struct Node *rightRotate(struct Node *y);

struct Node *leftRotate(struct Node *x);
 
int getBalance(struct Node *N);
void printInorder(struct Node* nodes,std::vector<string> &x,int contor,int k);
struct Node* insert(struct Node* , string movie_id,int timestamp);
void Search_in_range(struct Node *root,int start,int stop,std::vector<std::string> &V,int &contor);
void InOrder(struct Node *root,int contor,int k,std::vector<std::string> &x);
/*int findPositionPreOrder(
    struct Node *root,
    int targetPos,
    int curPos);
*/

#endif
