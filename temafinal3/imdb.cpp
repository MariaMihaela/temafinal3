#include <iterator>
#include <string>
#include <iostream>
#include <map>
#include <math.h>
#include <climits>
#include <vector>
#include <set>
#include <unordered_map>
#include <algorithm>    // std::sort
#include"version2_heap.h"
#include "imdb.h"
#include"avll.h"
#include"career_actor.h"
#include"structuri.h"
#include "time.h"
#include"pairs.h"

using namespace std;

std::map<std::string,Hactor> Hactormap;
std::map<std::string,Huser> Husermap;
std::map<std::string, std::pair<std::set<std::string>, int > > Directors;
std::map <std::string, std::map<int, std::vector<std::string> > > Categorii;
bool myfunction (career x,career y) { return (x.nume< y.nume); }
bool custom_comparator(std::pair<std::string, int> p1, std::pair<std::string, int> p2) {
	if (p1.second != p2.second) {
		return p1.second > p2.second;
	}
		else {
			return p1.first < p2.first;
		}
}


IMDb::IMDb() {
	root = NULL;
}
IMDb::~IMDb() {if (root!=NULL) delete root;}

void IMDb::add_movie(std::string movie_name,
                     std::string movie_id,
                     int timestamp,
                     std::vector<std::string> categories,
                     std::string director_name,
                     std::vector<std::string> actor_ids) {

		root = insert(root, movie_id,timestamp);
for(auto i=actor_ids.begin();i!=actor_ids.end();++i) {
	if(Hactormap[*i].Innr==0 && Hactormap[*i].Lnr==-1) {Hactormap[*i].Innr=Hactormap[*i].Lnr=timestamp;cout<<"hopaaa";}
	else {
		if(Hactormap[*i].Innr>timestamp){

		Hactormap[*i].Innr=timestamp;
		cout<<"adaugat"<<Hactormap[*i].Innr<<endl;
	}
		else if(Hactormap[*i].Lnr<timestamp) Hactormap[*i].Lnr=timestamp;
	}
} 





	//adaugare film in hashtabel general Movies
	Movies[movie_id].movie_name = movie_name; 
    Movies[movie_id].timestamp = timestamp;
    Movies[movie_id].categories.swap(categories);
    Movies[movie_id].director_name = director_name;
    Movies[movie_id].actor_ids.swap(actor_ids);
	//completare hashtable Directors si Hactormap cu parteneri
	for (auto i = Movies[movie_id].actor_ids.begin(); i != Movies[movie_id].actor_ids.end(); ++i) {
		Directors[director_name].first.insert(*i);
		Directors[director_name].second = Directors[director_name].first.size();
		
		for (auto j = Movies[movie_id].actor_ids.begin(); j != Movies[movie_id].actor_ids.end(); ++j ) {
			if (i != j) {
				int p = 0;
				if (Hactormap[*i].partners.size() != 0) {
					for (auto k = Hactormap[*i].partners.begin(); k != Hactormap[*i].partners.end(); ++k) {

						if ((*k).first == *j) {
							(*k).second ++;
							p = 1;
							break;
						}

					}
						if (p == 0) {
							std::pair<string, int> colaborare;
							colaborare = std::make_pair (*j,1);
							Hactormap[*i].partners.push_back(colaborare);
						}
				}
					else if (Hactormap[*i].partners.size() == 0){  
							std::pair<string, int> colaborare;
							colaborare = std::make_pair (*j,1);
							Hactormap[*i].partners.push_back(colaborare);


					}

			}
		}

	//for (auto k = Hactormap[*i].partners.begin(); k != Hactormap[*i].partners.end(); ++k)
	//cout<<(*k).first<<" "<<(*k).second<<endl;

	}
	//completare film hashtable categorii
	int year = get_year(timestamp);
	for (auto i = Movies[movie_id].categories.begin(); i!= Movies[movie_id].categories.end(); ++i) {
		Categorii[*i][year].push_back(movie_id);
	}
	

}

void IMDb::add_user(std::string user_id, std::string name) {

	Huser x;
	x.name = name;
	x.id = user_id;
	Husermap[user_id] = x;
} 	

void IMDb::add_actor(std::string actor_id, std::string name) { /******/
	//Hactormap[actor_id] = { name, "", "", 0, 0 };
	Hactormap[actor_id].name = name;
	Hactormap[actor_id].id=actor_id;
	
	
}

void IMDb::add_rating(std::string user_id, std::string movie_id, int rating) { /******/
	//+insert in heap
	Movies[movie_id].no_ratings ++;
	std::pair<std::string, int> entry = std::make_pair(user_id, rating);
	Movies[movie_id].ratings.push_back(entry);
	Movies[movie_id].sum += rating;
}
    //int Search(int nr,string nume,int poz,int rating_nou,int actiune);

void IMDb::update_rating(std::string user_id, std::string movie_id, int rating) {
	//tb scazut si adaugat ratingul din suma
	for (auto i = Movies[movie_id].ratings.begin();i !=Movies[movie_id].ratings.end();++i) {
		if ((*i).first == user_id) {
				Movies[movie_id].sum -= (*i).second;
				(*i).second = rating;
				Movies[movie_id].sum += rating;
		}
	}

	//int actiune=1;
	//Top_rating.Search(rating_vechi,Movies[movie_id].movie_name,0,Movies[movie_id].no_ratings,actiune);
	
	
	//cauta filmul unde s.a updatat ratingul. vezi daca e si in heap. daca e, update.
}

void IMDb::remove_rating(std::string user_id, std::string movie_id) {  /******/
//tb sters 
	for (auto i = Movies[movie_id].ratings.begin();i !=Movies[movie_id].ratings.end();++i) {
		if ((*i).first == user_id) {
				Movies[movie_id].sum -= (*i).second;
				Movies[movie_id].ratings.erase(i);
				Movies[movie_id].no_ratings --;
				break;
		}
	}



} 

//este
std::string IMDb::get_rating(std::string movie_id) {
float sum=0;

string s="none";
if(Movies[movie_id].no_ratings==0) sum=0;
else{
for(int i=0;i<Movies[movie_id].no_ratings;i++)  //S.AR PUTEA SA DEA SEG FAULT PT CA INCEPE DE LA 0 SI SE TERMINA CU <. NU STIU
	sum=sum+Movies[movie_id].ratings[i].second;
sum=sum/Movies[movie_id].no_ratings;
s = std::to_string(sum);
	//{cout<<Movies[movie_id].no_ratings<<endl;
	//	}
return s;}
return "none";

}
//NU MERGE COMMITUL
//nu mergee

std::string IMDb::get_longest_career_actor() {// me
//1. parcurg hashtable.ul de actori, pt ca sunt in O(1) si fac diferenta dintre filmul minim si maxim i
int contor=0;
int max=0;
std::vector<career> V;
for (auto it = Hactormap.begin() ; it != Hactormap.end(); ++it) {
	cout<<"inainte"<<endl<<(*it).second.id<<" "<<(*it).second.Innr<<" "<<(*it).second.Lnr<<endl;
	 if((*it).second.Lnr-(*it).second.Innr> max ) {V.clear();
							    contor=0;
							   career c;
							   c.nume=(*it).second.name;
							   c.id=(*it).second.id;
							   c.nr_ani=(*it).second.Lnr-(*it).second.Innr;
						            V.push_back(c);
							   max=c.nr_ani;
							    contor++;
								}
	else if((*it).second.Lnr-(*it).second.Innr == max ) { 
		career d;
		 d.nume=(*it).second.name;
		 d.id=(*it).second.id;
		 d.nr_ani=(*it).second.Lnr-(*it).second.Innr;
		 V.push_back(d);
		 contor ++;}
		cout<<"dupa"<<endl<<(*it).second.id<<" "<<(*it).second.Innr<<" "<<(*it).second.Lnr<<endl;
}
std::sort(V.begin(),V.end(),myfunction);
for (auto it = V.begin() ; it != V.end(); ++it) {
	cout<<(*it).id<<" "<<(*it).nume<<" "<<(*it).nr_ani<<endl;}
career x=V.front();
return x.id;

}
//***************************************************************************************************************/

std::string IMDb::get_most_influential_director() {
	
 	int nr_colaborari = 0;
	string name = "";
 	for ( auto& it : Directors) {
 				if (it.second.second > nr_colaborari) {
 					nr_colaborari = it.second.second;
 					name = it.first;
 				}
 					else if (it.second.second == nr_colaborari) {
 						if (it.first < name) name = it.first;
 					}
 				
 	}
 	
 	return name;

}

std::string IMDb::get_best_year_for_category(std::string category) { 
	 int max = 0;
	 string best_year = "";
	for (const auto &x : Categorii[category]) {
		int nr_filme = 0;
			float sum_ratings = 0;
				for (const auto &y : x.second) {
				nr_filme ++;
				string aux = IMDb::get_rating(y);
				
				double temp = ::atof(aux.c_str());
				if (isnan(temp)) {
					sum_ratings += 0;
				}
					else {
						sum_ratings += temp;
					}
			
			}


				sum_ratings = sum_ratings / nr_filme;
				

				if (sum_ratings > max) {
					max = sum_ratings;
					best_year=std::to_string(x.first);
				}
				 	else if (sum_ratings == max) {
				 		if (best_year == "") {
				 			best_year = std::to_string(x.first);
				 		}
				 		else {
				 			if (x.first < atoi(best_year.c_str())) {
				 			best_year = std::to_string(x.first);
				 			}
				 		}
				 	}
		}

				
	
	
	if (best_year == "") {
		return "none";
	}
		else {
			return best_year;
		}
}


std::string IMDb::get_2nd_degree_colleagues(std::string actor_id) {
	//cautam actorul care ne intereseaza in Hactors si ii parcurgem lista de vecini
	//directi, apoi parcugem lista de vecini ai celor directi
    std::set<std::string> colleagues;//vrem unicitate
    std::string result;
    for (auto i = Hactormap[actor_id].partners.begin(); i != Hactormap[actor_id].partners.end(); ++i) {
    	for (auto j = Hactormap[(*i).first].partners.begin(); j!= Hactormap[(*i).first].partners.end(); ++j) {
    		int p = 0;
    		     if((*j).first!= actor_id ){

    				for (auto k = Hactormap[actor_id].partners.begin(); k != Hactormap[actor_id].partners.end(); ++k)
 						if((*j).first == (*k).first && (*k).first != actor_id) {
 						p = 1; // ne de gradul 1
 						break;
 					}

    				if (p == 0) {
    				colleagues.insert((*j).first);
    				}
    			}
    		
    	}
    }
    
   //std::sort(colleagues.begin(), colleagues.end());
   for(auto i = colleagues.begin(); i!= colleagues.end(); ++i) {
   	
   		result += *i;
   		result += " ";
   		//result += " ";
   }
   
   if (colleagues.size() == 0) {
   		return "none";
   }
   		else {
   			return result;
   		}
}

std::string IMDb::get_top_k_most_recent_movies(int k) { 	
   std::vector<std::string> x; 
   x.push_back("0");
/*for (auto i = Movies.begin();i !=Movies.end();++i) {
	cout<<(*i).second.movie_name<<" "<<(*i).first<<" "<<(*i).second.timestamp<<endl;}*/
printInorder(root,x,0,k);
  string finall="";
int nr=x.size();
//cout<<nr-1<<"SIZE <---"<<endl;
if(nr-1<k) k=nr-1;

 for(int i=1;i<=k;i++)
	finall=finall+x[i]+" ";
cout<<endl;
cout<<finall<<endl;
cout<<endl;
cout<<nr-1<<"<-size , k-->"<<k<<endl;
    if(nr-1==0) return "none";
    else return finall;
}




std::string IMDb::get_top_k_actor_pairs(int k) {
std::vector<string> V;
Heap_pairs Pairs(k);
//Pairs.print();
cout<<"GET TOP MOST"<<k<<" POPULAR TIME"<<endl;
std::vector<std::tuple<int,string,string> > v;
//nu mai incarc degeaba pozitia 0 pt ca o sa am probleme la parcurgere
for(auto it=Hactormap.begin();it!=Hactormap.end();++it)
	for(auto j= (*it).second.partners.begin();j!=(*it).second.partners.end();++j)
		for(auto s=(*it).second.used.begin();s!=(*it).second.used.end();++s)//ma plimb prin vectorul de perechi tot din hashtable.ul Anei
		{	//acum verific daca perechea Anei din used e aceasi cu cea din partners
			if((*j).first==(*s).first && (*s).second==0) //daca am gasit perechea libera o adaug in heap;
				{Pairs.insert((*j).second/*de cate ori sunt in pereche*/ ,(*it).first /*id-ul primului*/ ,(*j).first);
				 //adaug id.urile partenerilor
				(*s).second=1; //in vectorul de used marchez ca perechea a fost deja introdusa 
				//acum trebuie sa marchez si in vectorul de used al celuilalt element din pereche ca sa nu am duplicate
				for( auto l=Hactormap[(*j).first].used.begin();l!=Hactormap[(*j).first].used.end();++l)
					{if((*l).first==(*it).first) {(*l).second=1;break;}} //in vectorul de used al celuilalt o caut pe Ana
				}	
		}


int dim=0;
int kk=k;
int kkk=kk;
int checker;
int am_ajuns_la_top=k;
while(k>0){
checker=Pairs.extract(v,dim);
if (checker==0) break;
//Top_Rating.print();
//cout<<endl;
k--;}

//am creat vectorul de id.uri 
//acum trebuie sa vad daca pot scoate k sau mai putine 
  string finall="";
int nr=v.size();
cout<<nr<<"numaaaarul de elemente din vector"<< endl;
//cout<<nr-1<<"SIZE <---"<<endl;
/*k=kk;
if(nr<k) k=nr;*/
int diferenta=am_ajuns_la_top-k-1;
for (auto it = v.begin() ; it != v.end(); ++it)
	if(diferenta>=0 ) {finall=finall+get<1>(*it)+" "+get<2>(*it)+"\n";diferenta--;}
		else break;
//cout<<endl;
cout<<finall<<"Finall"<<"k-->"<<kkk<<endl;
//cout<<endl;



/*cout<<"TOP RATING GUYS"<<kk<<endl;
for(auto it=v.begin();it!=v.end();++it)
	cout<<(*it).second<<" ";
*/
//cout<<nr-1<<"<-size , k-->"<<k<<endl;
Pairs.regenerare();
    if(nr==0) return "none";
    else return finall;
/*
for (auto it = Hactormap.begin(); it != Hactormap.end(); ++it) //pt fiecare individ
	for(auto i = Hactormap[(*it).first].partners.begin();i!=Hactormap[(*it).first].partners.end();++i) //cauta perechile\	
		{for(auto k=(*it).second.used.begin();k!=(*it).second.used.end();++k)
		{	
			if((*i).first==(*k).first && (*k).second==0) //daca am gasit perechea libera o adaug in heap;
				{Pairs.insert((*i).second ,(*it).first ,(*i).first);
				 //adaug id.urile partenerilor
				(*k).second=1;
				for( auto l=Hactormap[(*k).first].used.begin();l!=Hactormap[(*k).first].used.end();++l)
					{if((*l).first==(*it).first) {(*l).second=1;break;}}
				}	
		}
		
cout<<endl;	
		}	*/
			

cout<<"FACUT"<<endl;
return "none";

}

std::string IMDb::get_top_k_partners_for_actor(int k, std::string actor_id) {
	//cautam actorul care ne intereseaza in Hactors si ii sortam vectorul de perechi (partener, nr_colaborari)
/*	std:vector <std::pair<string, int> > top_partners;
	for (auto k = Hactormap[actor_id].partners.begin(); k!= Hactormap[actor_id].partners.end(); ++k) {
		top_partners.push_back(*k);

	}
    
	std::sort(top_partners.begin(), top_partners.end(), custom_comparator);
	//concatenare rezultat
	std::string top_k_partners = "";  
	if (k >= top_partners.size()) {
		for (auto i = top_partners.begin(); i!= top_partners.begin(); ++i)
			top_k_partners += *i.first;
			top_k_partners += " ";
	}
		else {
			for (auto i = top_partners.begin(); i != top_partners.begin() + k - 1; ++i) {
				top_k_partners += *i.first;
				top_k_partners += " ";
			}
		}

	if (top_k_partners == "") {
		return "";
	}
		else {
			return top_k_partners;
		}*/
	return "";
}


std::string IMDb::get_top_k_most_popular_movies(int k) { 
std::vector<string> V;
Heap Top_Rating(k);

cout<<"GET TOP MOST"<<k<<" POPULAR TIME"<<endl;
std::vector<std::pair<int,string> > v;
std::pair<int,string> naspa;
for (auto it = Movies.begin() ; it != Movies.end(); ++it){
cout<<endl;
cout<<(*it).second.no_ratings<<" "<<(*it).second.movie_name<<" "<<(*it).first<<"ADAUGAT"<<endl;
	Top_Rating.insert((*it).second.no_ratings,(*it).second.movie_name,(*it).first);}


int dim=0;
int kk=k;
int kkk=kk;
int checker;
int am_ajuns_la_top=k;
while(k>0){
checker=Top_Rating.extract(v,dim);
if (checker==0) break;
cout<<"TOP RATING GUYS"<<kk<<endl;
for(auto it=v.begin();it!=v.end();++it)
	cout<<(*it).second<<" "<<(*it).first<<endl;
Top_Rating.print();
cout<<endl;
k--;}

//am creat vectorul de id.uri 
//acum trebuie sa vad daca pot scoate k sau mai putine 
  string finall="";
int nr=v.size();

int diferenta=am_ajuns_la_top-k-1;
for (std::vector<std::pair<int,string> >::reverse_iterator i =v.rbegin(); 
        i != v.rend(); ++i ) { 
	if(diferenta>=0 ) {finall=finall+(*i).second+" ";diferenta--;}
		else break;}

Top_Rating.regenerare();
    if(nr==0) return "none";
    else return finall;

}
std::string IMDb::get_avg_rating_in_range(int start, int end) {std::vector<std::string> V; // fa-l resizable
	int contor=1;
	float suma=0;
	int number=0;
	float avarage;
	Search_in_range(root,start,end,V,contor);
	for(auto it=V.begin();it!=V.end();++it){ 
		suma=suma+Movies[*it].sum;
		number=number+Movies[*it].no_ratings;}
	if(number==0) return "none";		
	avarage=suma/number;
	cout<<suma<<" "<<number<<" "<<avarage<<" "<<endl;
	std::string avv;
	avv=to_string(avarage);
	//mai trebuie sa.l convertescu doua zecimale
    return avv;
}

