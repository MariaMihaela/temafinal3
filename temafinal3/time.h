#ifndef __TIME__H__
#define __TIME__H__
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include<iostream>

int get_year(int timestamp);

int get_mon(int timestamp);

int get_day(int timestamp);

#endif
