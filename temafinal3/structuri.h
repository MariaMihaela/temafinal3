#ifndef __STRUCTURI__H__
#define __STRUCTURI__H__
#include<string>
#include<iostream>
#include<vector>
#include<cstdlib>
#include"hashtable.h"
//#include"heap.h"

struct Hactor{
std::string name;
std::string id;
std::vector<std::pair<string, int> > partners;
std::vector<std::pair<string,int> > used;
int Innr=0; //timestampul cand a debutat
int Lnr=-1; //timestampul ultimului film.
	
};
struct Rating {
    	std::string movie_name;
    	std::string movie_id;
	int ratingg;
    	struct rating *left;
    	struct rating *right;
    	int height;
Rating(){
	ratingg=0;
	height=0;}
Rating(std::string movie_name, std::string movie_id, int rating){
 	left=nullptr;
	right=nullptr;
	this->movie_name=movie_name;
	this->movie_id=movie_id;
	this->ratingg=rating;
//ar mai tb sa fac ceva si cu height*/
}
Rating(const Rating &nou){ //copy constructor
	this->movie_name=nou.movie_name;
	this->movie_id=nou.movie_id;
	this->ratingg=nou.ratingg;
	this->height=nou.height;
}
Rating &operator=(const Rating& nou){
	this->movie_name=nou.movie_name;
	this->movie_id=nou.movie_id;
	this->ratingg=nou.ratingg;
	this->height=nou.height;

}
struct ajutor_operator{int x;
		    string y;};
 friend bool operator< (const Rating &x, const Rating& y);
 friend bool operator> (const Rating& x, const Rating& y);
 friend std::ostream& operator<<(std::ostream &f,const Rating& nou);


};
std::ostream& operator<<(std::ostream& f, const Rating& nou)
{
    // write obj to stream
  f<<nou.ratingg;
return f;

}
 //std::ostream& operator<<(std::ostream& stream,const Rating& nou)

 bool operator<(const Rating &x, const Rating& y)
    {
        return (x.ratingg
             < y.ratingg); // keep the same order
    }
 bool operator>(const Rating& x, const Rating& y)
    {
        return (x.ratingg
             > y.ratingg); // keep the same order
    }
bool operator<=(const Rating &x, const Rating& y)
    {
        return (x.ratingg
             <= y.ratingg); // keep the same order
    }
 bool operator>=(const Rating& x, const Rating& y)
    {
        return (x.ratingg
             >= y.ratingg); // keep the same order
    }
struct Actor_heap{
std::string name;
int dif;
int height;
struct Node *left;
struct Node *right;
Actor_heap(){dif=0;
	height=0;}
Actor_heap(std::string movie_name,int dif){
	left=NULL;
	right=NULL;
	this->name=movie_name;
	this->dif=dif;
//ar mai tb sa fac ceva si cu height

}
Actor_heap(const Actor_heap& nou){ //copy constructor
	this->name=nou.name;
	this->dif=nou.dif;
	this->height=nou.height;
}
Actor_heap& operator=(const Actor_heap& nou){
	this->name=nou.name;
	this->dif=nou.dif;
	this->height=nou.height;
	}
friend bool operator< (const Actor_heap& x, const Actor_heap& y);
friend bool operator> (const Actor_heap& x, const Actor_heap& y);
};
bool operator<(const Actor_heap& x, const Actor_heap& y)
    {
        return (x.dif< y.dif); // keep the same order
    }
 bool operator>(const Actor_heap& x, const Actor_heap& y)
    {
        return (x.dif> y.dif); // keep the same order
    }

struct Huser{
std::string name;
std::string id;
std::vector<std::string> ratings;
std::vector<std::string> films;	
	Huser(){}
	Huser(const Huser& nou){ //copy constructor
	this->name=nou.name;
	this->id=nou.id;
	this->ratings=nou.ratings;
	this->films=nou.films;
	}
	Huser& operator=(const Huser&nou){//copy assigment
	this->name=nou.name;
	this->id=nou.id;
	this->ratings=nou.ratings;
	this->films=nou.films;}
	

};
#endif



