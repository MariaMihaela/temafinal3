#include<stdio.h>
#include<stdlib.h>
#include<vector>
#include<fstream>
#include<iostream>
using namespace std;
 struct Node *root;
struct Node{
    int key;
    struct Node *left;
    struct Node *right;
    int height;
};
 


void preOrder(struct Node *root)
{
    if(root != NULL)
    {
        preOrder(root->left);
	  printf("%d ", root->key);
        preOrder(root->right);
    }
}



int max(int a, int b);

int height(struct Node *N){
    if (N == NULL)
        return 0;
    return N->height;
}

int max(int a, int b) {
    return (a > b)? a : b;
}
 

struct Node* newNode(int key) {
    struct Node* node = new struct Node;
    node->key   = key;
    node->left   = NULL;
    node->right  = NULL;
    node->height = 1;  
    return(node);
}
  

struct Node *rightRotate(struct Node *y) {
    struct Node *x = y->left;
    struct Node *T2 = x->right;
 
  
    x->right = y;
    y->left = T2;
 
    y->height = max(height(y->left), height(y->right))+1;
    x->height = max(height(x->left), height(x->right))+1;
 
    return x;
}
 

struct Node *leftRotate(struct Node *x)
{
    struct Node *y = x->right;
    struct Node *T2 = y->left;

    y->left = x;
    x->right = T2;
 
  
    x->height = max(height(x->left), height(x->right))+1;
    y->height = max(height(y->left), height(y->right))+1;
 
    return y;
}
 
int getBalance(struct Node *N)
{
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}
 

struct Node* insert(struct Node* node, int key)
{
    if (node == NULL){	preOrder(root);
			cout<<endl;
        return(newNode(key));
	}
 
    if (key <= node->key){
        node->right  = insert(node->right, key);
		preOrder(root);
		cout<<endl;
	}
    else if (key >= node->key){
        node->left = insert(node->left, key);
	preOrder(root);
	cout<<endl;
	}
    else
        return node;
 
    node->height = 1 + max(height(node->left),
                           height(node->right));
 
    int balance = getBalance(node);

    if (balance > 1 && key >= node->left->key)
        return rightRotate(node);
 
    if (balance < -1 && key <= node->right->key)
        return leftRotate(node);
 
    if (balance > 1 && key <= node->left->key)
    {
        node->left =  leftRotate(node->left);
        return rightRotate(node);
    }
 
     if (balance < -1 && key >= node->right->key)
    {
        node->right = rightRotate(node->right);
        return leftRotate(node);
    }
 
    return node;
}
 

 

int findPositionPreOrder(
    struct Node *root,
    int targetPos,
    int curPos)
{
    if(root != NULL)
    {
        int newPos = findPositionPreOrder(root->left, targetPos, curPos);
        newPos++;
        if (newPos == targetPos)
        {
            printf("%d\n", root->key);
        }
        return findPositionPreOrder(root->right, targetPos, newPos);
    }

    else
    {
        return curPos;
    }
}
/* 
int/*nuj de ce tip e Search_in_range(struct Node *root,int start,int stop){
//1.transforma timestampul in an;
//2.cauta daca e in range
//3.returneaza id.ul filmului in range
/*if(root==nullptr) return
int y=root->key;
cout<<"UITE"<<root->key<<endl;
if(y>=start && y<= stop) {cout<<contor<<endl;
	V[contor]=y;contor++;
	if(root->left->key>=start) return Search_in_range(root->left,start,stop,V,contor);
	if(root->right->key<=start) return Search_in_range(root->right,start,stop,V,contor);
} 
else if(y<=start) return Search_in_range(root->right,start,stop,V,contor);
else if(y>=stop) return Search_in_range(root->left,start,stop,V,contor); 

}*/
void Search_in_range(struct Node *root,int start,int stop,string V[1000],int &contor)
{
if(root!=nullptr){
if(root->key>=start && root->key <=stop)
	{V[contor]="A";contor++;}
if(root->right!=nullptr) Search_in_range(root->right,start,stop,V,contor);
if(root->left!=nullptr)  Search_in_range(root->left,start,stop,V,contor);

}
}

int main()
{
 root = NULL;
 long int x;
 ifstream f("numbers.txt");
  for (int i = 1;i <=10; i++){
	f >> x;
  root = insert(root, x);
  }

 // for (int i=1;i<=500;i++)
//	findPositionPreOrder(root,i,1);
 
  preOrder(root);
string V[100];
int contor=1;
cout<<endl;
Search_in_range(root,1,25,V,contor);
for (int i=1;i<contor;i++)
	cout<<V[i]<<' ';
cout<<endl;
 
  return 0;
}
